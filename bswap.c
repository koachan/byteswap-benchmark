#include <stdint.h>

uint16_t u16_bswap_builtin(uint16_t n) {
    return __builtin_bswap16(n);
}

uint32_t u32_bswap_builtin(uint32_t n) {
    return __builtin_bswap32(n);
}

uint64_t u64_bswap_builtin(uint64_t n) {
    return __builtin_bswap64(n);
}

uint16_t u16_bswap_shift(uint16_t n) {
    union {
        uint16_t i;
        uint8_t  b[2];
    } u; u.i = n;
    return (uint16_t) u.b[1] <<  8 | (uint16_t) u.b[0];
}

uint32_t u32_bswap_shift(uint32_t n) {
    union {
        uint32_t i;
        uint8_t  b[4];
    } u; u.i = n;
    return (uint32_t) u.b[3] << 24 | (uint32_t) u.b[2] << 16 | (uint32_t) u.b[1] <<  8 | (uint32_t) u.b[0];
}

uint64_t u64_bswap_shift(uint64_t n) {
    union {
        uint64_t i;
        uint8_t  b[8];
    } u; u.i = n;
    return (uint64_t) u.b[7] << 56 | (uint64_t) u.b[6] << 48 | (uint64_t) u.b[5] << 40 | (uint64_t) u.b[4] << 32
         | (uint64_t) u.b[3] << 24 | (uint64_t) u.b[2] << 16 | (uint64_t) u.b[1] <<  8 | (uint64_t) u.b[0];
}

uint16_t u16_bswap_sta(uint16_t n) {
    uint16_t s;
    asm volatile ("stha %1, [%1] #ASI_P_L\n\t"
                  "lduha [%1] #ASI_P, %0"
                  : "=r" (n) : "r" (&s), "r" (n) : "memory");
    return n;
}

uint32_t u32_bswap_sta(uint32_t n) {
    uint32_t s;
    asm volatile ("stwa %2, [%1] #ASI_P_L\n\t"
                  "lduwa [%1] #ASI_P, %0"
                  : "=r" (n) : "r" (&s), "r" (n) : "memory");
    return n;
}

uint64_t u64_bswap_sta(uint64_t n) {
    uint64_t s;
    asm volatile ("stxa %2, [%1] #ASI_P_L\n\t"
                  "ldxa [%1] #ASI_P, %0"
                  : "=r" (n) : "r" (&s), "r" (n) : "memory");
    return n;
}
