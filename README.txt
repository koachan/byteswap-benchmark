Run it with `CC=[yourcompiler] make clean speed`.

Some speed result on SPARC T2 running Solaris 11.3:

u16_bswap_builtin: 26.131210 MiB/s
u16_bswap_shift:   49.740870 MiB/s
u16_bswap_sta:     74.403345 MiB/s
u32_bswap_builtin: 50.952195 MiB/s
u32_bswap_shift:   93.589774 MiB/s
u32_bswap_sta:     136.258197 MiB/s
u64_bswap_builtin: 96.120721 MiB/s
u64_bswap_shift:   169.358335 MiB/s
u64_bswap_sta:     235.529093 MiB/s
