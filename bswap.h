#ifndef BSWAP_H
#define BSWAP_H

#include <stdint.h>

uint16_t u16_bswap_builtin(uint16_t n);
uint32_t u32_bswap_builtin(uint32_t n);
uint64_t u64_bswap_builtin(uint64_t n);

uint16_t u16_bswap_shift(uint16_t n);
uint32_t u32_bswap_shift(uint32_t n);
uint64_t u64_bswap_shift(uint64_t n);

uint16_t u16_bswap_sta(uint16_t n);
uint32_t u32_bswap_sta(uint32_t n);
uint64_t u64_bswap_sta(uint64_t n);

#endif // BSWAP_H
