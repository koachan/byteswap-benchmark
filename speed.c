#include <stdint.h>
#include <stdio.h>
#include <time.h>

#include "bswap.h"

#define SIZE 4 * 1024 // 4 KiB
#define NS ((double) 1000000000)

#if defined(sun) || defined(__sun)
  #define CLOCK_ID CLOCK_HIGHRES
#else
  #define CLOCK_ID CLOCK_PROCESS_CPUTIME_ID
#endif /* defined(sun) || defined(__sun) */

typedef uint64_t duration_t;

// Duration in ns
duration_t duration(struct timespec start, struct timespec end) {
    return (end.tv_sec - start.tv_sec) * NS + (end.tv_nsec - start.tv_nsec);
}

// Speed in MiB/s
double speed(duration_t ns) {
    return (SIZE * NS) / (ns * 1024 * 1024);
}

duration_t speed_u16_bswap_builtin() {
    uint16_t in[SIZE/sizeof(uint16_t)] = {128};
    uint16_t out[SIZE/sizeof(uint16_t)];
    struct timespec start, end;
    int i;

    clock_gettime(CLOCK_ID, &start);
    for(i = 0; i < SIZE/sizeof(uint16_t); i++)
        out[i] = u64_bswap_builtin(in[i]);
    clock_gettime(CLOCK_ID, &end);
    return duration(start, end);
}

duration_t speed_u32_bswap_builtin() {
    uint32_t in[SIZE/sizeof(uint32_t)] = {128};
    uint32_t out[SIZE/sizeof(uint32_t)];
    struct timespec start, end;
    int i;

    clock_gettime(CLOCK_ID, &start);
    for(i = 0; i < SIZE/sizeof(uint32_t); i++)
        out[i] = u64_bswap_builtin(in[i]);
    clock_gettime(CLOCK_ID, &end);
    return duration(start, end);
}

duration_t speed_u64_bswap_builtin() {
    uint64_t in[SIZE/sizeof(uint64_t)] = {128};
    uint64_t out[SIZE/sizeof(uint64_t)];
    struct timespec start, end;
    int i;

    clock_gettime(CLOCK_ID, &start);
    for(i = 0; i < SIZE/sizeof(uint64_t); i++)
        out[i] = u64_bswap_builtin(in[i]);
    clock_gettime(CLOCK_ID, &end);
    return duration(start, end);
}

duration_t speed_u16_bswap_shift() {
    uint16_t in[SIZE/sizeof(uint16_t)] = {128};
    uint16_t out[SIZE/sizeof(uint16_t)];
    struct timespec start, end;
    int i;

    clock_gettime(CLOCK_ID, &start);
    for(i = 0; i < SIZE/sizeof(uint16_t); i++)
        out[i] = u64_bswap_shift(in[i]);
    clock_gettime(CLOCK_ID, &end);
    return duration(start, end);
}

duration_t speed_u32_bswap_shift() {
    uint32_t in[SIZE/sizeof(uint32_t)] = {128};
    uint32_t out[SIZE/sizeof(uint32_t)];
    struct timespec start, end;
    int i;

    clock_gettime(CLOCK_ID, &start);
    for(i = 0; i < SIZE/sizeof(uint32_t); i++)
        out[i] = u64_bswap_shift(in[i]);
    clock_gettime(CLOCK_ID, &end);
    return duration(start, end);
}

duration_t speed_u64_bswap_shift() {
    uint64_t in[SIZE/sizeof(uint64_t)] = {128};
    uint64_t out[SIZE/sizeof(uint64_t)];
    struct timespec start, end;
    int i;

    clock_gettime(CLOCK_ID, &start);
    for(i = 0; i < SIZE/sizeof(uint64_t); i++)
        out[i] = u64_bswap_shift(in[i]);
    clock_gettime(CLOCK_ID, &end);
    return duration(start, end);
}

duration_t speed_u16_bswap_sta() {
    uint16_t in[SIZE/sizeof(uint16_t)] = {128};
    uint16_t out[SIZE/sizeof(uint16_t)];
    struct timespec start, end;
    int i;

    clock_gettime(CLOCK_ID, &start);
    for(i = 0; i < SIZE/sizeof(uint16_t); i++)
        out[i] = u64_bswap_sta(in[i]);
    clock_gettime(CLOCK_ID, &end);
    return duration(start, end);
}

duration_t speed_u32_bswap_sta() {
    uint32_t in[SIZE/sizeof(uint32_t)] = {128};
    uint32_t out[SIZE/sizeof(uint32_t)];
    struct timespec start, end;
    int i;

    clock_gettime(CLOCK_ID, &start);
    for(i = 0; i < SIZE/sizeof(uint32_t); i++)
        out[i] = u64_bswap_sta(in[i]);
    clock_gettime(CLOCK_ID, &end);
    return duration(start, end);
}

duration_t speed_u64_bswap_sta() {
    uint64_t in[SIZE/sizeof(uint64_t)] = {128};
    uint64_t out[SIZE/sizeof(uint64_t)];
    struct timespec start, end;
    int i;

    clock_gettime(CLOCK_ID, &start);
    for(i = 0; i < SIZE/sizeof(uint64_t); i++)
        out[i] = u64_bswap_sta(in[i]);
    clock_gettime(CLOCK_ID, &end);
    return duration(start, end);
}

int main() {
    printf("u16_bswap_builtin: %lf MiB/s\n", speed(speed_u16_bswap_builtin()));
    printf("u16_bswap_shift:   %lf MiB/s\n", speed(speed_u16_bswap_shift()));
    printf("u16_bswap_sta:     %lf MiB/s\n", speed(speed_u16_bswap_sta()));
    printf("u32_bswap_builtin: %lf MiB/s\n", speed(speed_u32_bswap_builtin()));
    printf("u32_bswap_shift:   %lf MiB/s\n", speed(speed_u32_bswap_shift()));
    printf("u32_bswap_sta:     %lf MiB/s\n", speed(speed_u32_bswap_sta()));
    printf("u64_bswap_builtin: %lf MiB/s\n", speed(speed_u64_bswap_builtin()));
    printf("u64_bswap_shift:   %lf MiB/s\n", speed(speed_u64_bswap_shift()));
    printf("u64_bswap_sta:     %lf MiB/s\n", speed(speed_u64_bswap_sta()));
    return 0;
}
