CFLAGS=-O2 -funroll-loops -m64 -std=gnu99
LDFLAGS=-m64

bswap: speed.o bswap.o
	$(CC) $(LDFLAGS) $^ -o $@

.PHONY: clean speed
clean:
	rm -f *.o bswap

speed: bswap
	./bswap
